* $ ls - lists files in directory

    * -a lists files and directories including hidden
    * -l lists in long format
    * -t orders by time last modified

   Multiple options can be used together for example ls -alt which lists
   all in long format in the order they were last modified.

* $ pwd - print working directory
* $ cd - change directory
* $ cd .. - takes you up a directory
* $ mkdir - makes a directory
* $ touch = creates new file, takes file name as argument.
* $ cp - copies file

    * example:
  
```bash
  $ cp source/file.txt destination/
```

*  An asterisk can be used to select groups of files (wildcards)

    * example:

```bash
  $ cp m*.txt
```

   This would copy files starting with m and ending in .txt 

 * $ mv - moves files

    * example:

```bash
  $ mv file.txt directory/
```

  This would move file.txt into directory

* $ mv can also be used to rename files

```bash
  $ mv batman.txt spiderman.txt
```

  This would rename batman.txt to spiderman.txt

* $ rm - removes files and directories

    * $ rm -r removes a directory and all its child directories
    * **THIS CAN'T BE UNDONE SO NEED TO BE CAREFUL**

* $ echo - accepts a string as standard input and echoes string to terminal as output
* $ > redirectos standard output to a file
* $ cat outputs the contents of a file to the terminal
* $ >> takes standard output of a command on the left and adds it to the file on the right
* $ < tajes standard input from a file and puts it into a program
* $ | - pipe

  * Takes the standard output of a command on the left and pipes it as standard input to command on the right
  * It's like a command to command

* $ wc - outputs the number of lines/words/characters
* $ sort - sorts alphabetically

* $ grep - global regular expression print

  * searches for patterns and returns the results
  * case sensitive
  * Options

    * -i makes case insensitive
    * -R searches all files in directory, outputs filenames and lines containing matched result
    * -Rl searches all files in directory, outputs filenames only

* $ sed - steam editor, accepts standard input, modifies based on expression before displaying as output
  
  * It's like find and replace

  * example:

```bash
  $ sed 's/snow/rain/' file.txt
```

  Snow is being found and replaced with rain in file.txt

```bash
  $ sed 's/snow/rain/g' file.txt
```

  All incidences of snow will be replaced with rain in file.txt

**Nano**

  * command line text editor works a bit like a desktop text editor
  * $ nano hello.txt opens a new tex file named hello

    * ctrl(^)O = save
    * ctrl + X = exit
    * ctrl + G = help

clear = clears terminal

* $ cd ~ takes you back to home directory

* $ df -h shows the free space 