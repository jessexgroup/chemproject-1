### 2017-11-21 Meeting ###

**Questions**

**What is discussed?**

* Next meeting arranged for 12th December 2017, 11:00
* Fourier Transform code, should be getting a sharper peak when applied to Harmonic Oscillator
* New things to do

**To Do:**

* Continue reading Molecular Modelling Principles and Applications (Leach)
* Continue with Python labs
* Lennard Jones code
* HMC code
* Combination of sin waves, apply FT and then use digital filter
* Run MD of water, FT on velocities, digital filter
* ~~Increase time of Harmonic Oscillator to get sharper peaks in FT~~
* ~~Change the sampling rate to see what happens to the FT~~