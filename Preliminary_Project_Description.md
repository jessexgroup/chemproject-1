## Preliminary Project Description ##

Due: 1st November 2017

**Reading**

MCMC Using Hamiltonian Dynamics

**Project Description:**

The positions and velocities from simulations will be stored and filtered using a digital filter. The digital filter takes signals and either enhances or eliminates information from them in a frequency selective way. This signal processing allows the enhancement of low frequency modes which come about from things such as changes in the conformation of a molecule. Giving the molecules a boost should make the transitions associated with the low frequency modes occur more easily making them easier to see.

Newton's laws of motion can be formalised as Hamiltonian dynamics. The equations from which need to be approximated for computer implementation which is done by discretising the time component, often done by using a small stepsize in the chosen integration method, an example of which is the velocity verlet. When molecular dynamics (MD) is used in combination with the Monte Carlo method, the method is called Hybrid Monte Carlo (HMC) which is also known as the Hamiltonian Monte Carlo method. HMC samples from the canonical distribution (Boltzmann distribution) where the Hamiltonian function, H(**q**,**p**) can be written as the sum of the potential and kinetic energies. The probability density is given by:

P(**q**,**p**) = (1/Z)exp(-U(**q**)/KBT)exp(-K(**p**)/KBT)

Here T is the temperature, KB is the Boltzmann constant, Z is a normalising constant (a partition function), U(**q**) is the potential energy, a function of the position of the molecule and K(**p**) is the kinetic energy, a function of the molecules momentum (its mass multiplied by its velocity). HMC has two main steps, the first of which randomly draws a new value for the momentum variable from a Gaussian distribution. Then in the second step Hamiltonian dynamics is used to propose a new state. This proposed state is either accepted or rejected. If accepted the proposed state is used as the next state and if rejected the current state is used again and new velocities are drawn from the Gaussian distribution. [1]

HMC sampling from a bimodal Gaussian distribution instead of a gaussian distribution should lead to low frequency vibrations being amplified, however this has turned out to be inefficient as after a certain point the simulation performance returns to a state where it is essentially sampling from the unimodal Gaussian. A series of "normal" and "altered" simulations (that sample from a bimodal distribution) will be run for the same molecule and then analysed to see how modifications in the "altered" simulations change the vibrational frequencies in the spectra produced by analysing the MD trajectories. A fourier analysis will be done on the velocities from the MD simulations to look at the frequencies.

**References:**

1. Radford M. Neal, MCMC using Hamiltonian dynamics, in Steve Brooks, Andrew Gelman, Galin Jones, Xiao-Li Meng. *Handbook of Markov Chain Monte Carlo*, Chapman & Hall/CRC Press, 2011




**To Do:**

* Carry on reading MCMC and Leach
* ~~Carry on drafting the statement~~
* ~~Hand in draft to Jon on Monday~~
* Sort questions I have noted into things that can be googled, looked up in books, or need to be asked

**Feedback/information to apply to the 500 words**

* Simulations using Hamiltonian dynamics
* MCMC does not give trajectory
* MD and Hamiltonian dynamics are used to simulate
* HMC samples
* Do not need to mention Metropolis here (f1)
* Accepted/rejected using Metropolis acceptance test
* only position is used again, velocities are always generated randomly

* Filter is applied to velocities
* Velocities and positions are stored
