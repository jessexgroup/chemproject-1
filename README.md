### What is this repository for? ###

The repo, contains any documents, code related to the Chemistry project.

**Resources:**

* Python Primer
* Bash/Shell Primer
* git primer
* OpenMM Tutorials and scripts
* Markdown primer

### How do I get set up? ###

**cloning the repository to your local machine:**

Type the following into your terminal

```bash
git clone https://CSPer@bitbucket.org/CSPer/chemproject.git
```

The command will copy the repository under the current working directory

**Adding changed/created files to the repository to be committed**

```bash
git add <filaname>
```

The command will stage the given files such that when commit command is executed it will be saved to the repository.

**Committing changes to the repository**

```bash
git commit
```

This command will commit staged files. 
After the command is executed a text editor will open,  a message should be written explaining the changes made.

**Pushing the commit/commits to the cloud**

```bash
git push origin master
```

This command will send all the commits to the online repository.


### Who do I talk to? ###

* Can Simon Pervane
* Matthew Brown